Parse.Cloud.define('checkUniqueness', function (req, res) {
  Parse.Cloud.useMasterKey();
  var params = req.params;
  var lat = params.lat;
  var lng = params.lng;
  var range = params.range;
  var areaObjID = params.areaID;
  var InterestID = params.InterestAreaID;
  var UserID = params.UserID;
  var name = params.Name;
  var user = new Parse.User();
  user.id = UserID;
  var userGeoPoint = new Parse.GeoPoint({ latitude: lat, longitude: lng });
  var InterestArea = new Parse.Object("InterestArea");
  InterestArea.id = InterestID;
  var Group = Parse.Object.extend('Group');
  var query = new Parse.Query(Group)
  query.withinKilometers("LocationPoint", userGeoPoint, range + 2);
  query.equalTo("GroupInterest", InterestArea);
  if(params.isPublic==0){
    query.equalTo("GroupInitiator",user);
    query.equalTo("IsPublic",0);
  }
  query.notEqualTo("Status", "disabled");
  query.find({
    success: function (results) {
      if (results.length > 0) {
        var flag = true;
        for (i = 0; i < results.length; i++) {
          var test = getDistanceFromLatLonInKm(lat, lng, results[i].get("LocationPoint")._latitude, results[i].get("LocationPoint")._longitude);
          console.info("Actual Distance:" + test);
          var test1 = range + results[i].get("Radius");
          if (test <= test1) {
            flag = false;
          } else {
            console.info("Non Intersecting Continue");
          }
          if (!flag) {
            res.success({"saved":false, "Unique": false });
          }
          else {

            var group = new Group();
            group.set("LocationPoint", userGeoPoint);
            group.set("GroupInitiator", user);
            group.set("GroupName", name);
            group.set("GroupInterest", InterestArea);
            group.set("Radius", range);
            if(params.isPublic==0)
              group.set("Status", "ok");
            else 
              group.set("Status", "new");
            group.set("IsPublic", params.isPublic);
            group.save(null,{
              success:function(data){
                if(params.isPublic==0)
                {
                  user.addUnique("PrivateGroups",data);
                  user.save(null,{
                    success:function(done){
                      res.success({ "saved": true,"GroupID":data.id,"IsPublic":params.isPublic })     
                    }
                  })
                } else {
                  res.success({ "saved": true,"GroupID":data.id,"IsPublic":params.isPublic })
                }
              }
            })
          }
        }
      } else {
        var group = new Group();
        group.set("LocationPoint", userGeoPoint);
        group.set("GroupInitiator", user);
        group.set("GroupName", name);
        group.set("GroupInterest", InterestArea);
        group.set("Radius", range);
        group.set("IsPublic", params.isPublic);
        if(params.isPublic==0)
          group.set("Status", "ok");
        else 
          group.set("Status", "new");
        group.save({
          success:function(data){
            if(params.isPublic==0)
            {
              user.addUnique("PrivateGroups",data);
              user.save(null,{
                success:function(done){
                  res.success({ "saved": true,"GroupID":data.id,"IsPublic":params.isPublic })     
                }
              })
            } else {
              res.success({ "saved": true,"GroupID":data.id,"IsPublic":params.isPublic })
            }
          }
        })
      }
    }
  });

});

Parse.Cloud.define('GroupChat', function (req, res) {
  var params = req.params;
  var firebase = require('firebase');
  var FCM = require('fcm-node');
  var newmessage ={
    message: params.message,
    uname: params.uname,
    uid: params.uid,
    timestamp: params.tstamp
  };
  var serverKey = 'AIzaSyBPNgYV-kc5r7qWxt_XI88Zrl2IZutqfGU';
  var fcm = new FCM(serverKey);
  var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
    to: '/topics/'+params.groupId, 
    collapse_key: params.groupId,
    notification: {
        title: 'New message from '+params.GroupName, 
        body: params.uname+":"+params.message 
    },
    data: message
  };
  fcm.send(message, function(err, response){
    if (err) {
        console.log("Something has gone wrong!");
    } else {
        console.log("Successfully sent with response: ", response);
    }
});

  var groupRef = firebase.database().ref('/group/' + params.groupId + '/messages');
  groupRef.push(newmessage)
  res.success({ success: true });
});

Parse.Cloud.define('subscribe', function (req, res) {
  Parse.Cloud.useMasterKey()
  var params = req.params;
  var Group = Parse.Object.extend('Group');
  var query = new Parse.Query(Group);
  query.equalTo("objectId", params.groupId);
  query.find({
    success: function (result) {
      if (result.length == 1) {
        var selectedGroup = result[0];
        var subscriber = new Parse.User();
        subscriber.id = params.userId;
        subscriber.addUnique("UserSubscriptions",selectedGroup);
        subscriber.save(null,{
          success:function(done){
            selectedGroup.addUnique("Subscribers", subscriber);
            selectedGroup.save(null, {
              success: function (done) {
                res.success({ done: true });
              }
            })
          }
        })
      }
    }
  })
});

Parse.Cloud.define('addprivate', function (req, res) {
  Parse.Cloud.useMasterKey()
  
  var params = req.params;
  var User = Parse.Object.extend('User');
  var Group = Parse.Object.extend('Group');
  var query = new Parse.Query(User);
  query.equalTo("objectId", params.userId);
  query.find({
    success: function (result) {      
      if (result.length == 1) {
        var selectedUser = result[0];
        var group = new Group();
        group.id = params.groupId;
        selectedUser.addUnique("PrivateGroups",group);
        selectedUser.save(null,{
          success:function(done){
             res.success({ done: true }); 
          }
        })
      }
    }
  })
});

Parse.Cloud.define('unSubscribe', function (req, res) {
   Parse.Cloud.useMasterKey();
  var params = req.params;
  var Group = Parse.Object.extend('Group');
  var query = new Parse.Query(Group);
  query.equalTo("objectId", params.groupId);
  query.include("Subscribers");
  query.find({
    success: function (result) {
      if (result.length == 1) {
        var selectedGroup = result[0];
        var subscriber = new Parse.User();
        subscriber.id = params.userID;
        subscriber.fetch();



        var subscribers = result[0].get("Subscribers");
        selectedGroup.set("Subscribers", []);
        for (i = 0; i < subscribers.length; i++) {
          if (subscribers[i].id != params.userID) {
            selectedGroup.addUnique("Subscribers", subscribers[i]);
          }
        }
        selectedGroup.save(null, {
          success: function (done) {
            var subscriptions = subscriber.get("UserSubscriptions");
            subscriber.set("UserSubscriptions",[]);
            
            for(i = 0;i<subscriptions.length;i++){
              if(subscriptions[i].id!=params.groupId){
                subscriber.addUnique("UserSubscriptions",subscriptions[i])
              }
            }
            subscriber.save(null,{
              success:function(result){
                res.success({ done: true });
              }
            })
          }
        })
      }
    }
  })
});


Parse.Cloud.define('removePrivate', function (req, res) {
   Parse.Cloud.useMasterKey();
  var params = req.params;
  var User = Parse.Object.extend('User');
  var query = new Parse.Query(User);
  query.equalTo("objectId", params.userId);
  //query.include("PrivateGroups");
  query.find({
    success: function (result) {
      if (result.length == 1) {
        var selectedUser = result[0];
        
        var PrivateGroups = result[0].get("PrivateGroups");
        selectedUser.set("PrivateGroups", []);
        for (i = 0; i < PrivateGroups.length; i++) {
          if (PrivateGroups[i].id != params.groupId) {
            selectedUser.addUnique("PrivateGroups", PrivateGroups[i]);
          }
        }
        selectedUser.save(null, {
          success: function (done) {
            res.success({ done: true });
         
          }
        })
      }
    }
  })
});



Parse.Cloud.define('reportPost', function (req, res) {
  var params = req.params;
  var TimelinePost = Parse.Object.extend('TimelinePost');
  var query = new Parse.Query(TimelinePost);
  query.equalTo("objectId", params.postId);
  query.find({
    success: function (result) {
      if (result.length == 1) {
        var tpost = result[0];
        var reporter = new Parse.User();
        reporter.id = params.userId;
        tpost.addUnique("Reporters", reporter);
        tpost.save(null, {
          success: function (updatedPost) {
            console.log(updatedPost.get("Reporters").length);
            if (updatedPost.get("Reporters").length > 1) {
              updatedPost.set("Visibility", 0);
              console.log("special");
              updatedPost.save(null, {
                success: function (finalData) {
                  res.send({ "success": true, "reload": true });
                }
              })
            } else {
              res.send({ "success": true, "reload": false });
            }

          }
        })
      } else {
        res.success({ "success": false, "reason": "Post ID Missmatch" });
      }
    }
  })
});

Parse.Cloud.define('supportInitative', function (req, res) {
  var params = req.params;
  var Group = Parse.Object.extend('Group');
  var query = new Parse.Query("Group");
  query.equalTo("objectId", params.groupId);
  query.include("GroupInterest");
  query.find({
    success: function (result) {
      if (result.length == 1) {
        var targetGroup = result[0];
        if (targetGroup.get("Status") == "disabled") {
          res.success({ success: false, "message": "Group is deleted, you cannot support it." });
        } else if (targetGroup.get("Status") == "ok") {
          res.success({ success: false, "message": "The Group has already got enough support." });
        } else if (targetGroup.get("Status") == "new") {
          if (targetGroup.get("GroupInitiator").id == params.userId) { //Change it to equal equal condition
            res.success({ success: false, "message": "You cannot vote for your group" });
          } else {
            console.error(result.length);
            var supporter = new Parse.User();
            supporter.id = params.userId;
            console.log(supporter);
            targetGroup.addUnique("Supporter", supporter);
            targetGroup.save(null, {
              success: function (data) {
                if (data.get("Supporter").length >= (targetGroup.get("GroupInterest").get("Threshold")) - 1) {
                  data.set("Status", "ok");
                  data.save(null,
                    {
                      success: function (updatedGroup) {
                        res.success({ success: true, reload: true });
                      }
                    });
                } else {
                  res.success({ success: true, reload: false });
                }
              },
              error: function (data, err) {
                res.success({ success: false });
              }
            })
          }
        } else
          res.success({ success: false, "message": "Undefined Error Occured" });
      } else {
        res.success({ success: false, "message": "Undefined Error Occured" });
      }
    }
  })
});

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}
function getUser(userId) {
  Parse.Cloud.useMasterKey();
  var userQuery = new Parse.Query(Parse.User);
  userQuery.equalTo("objectId", userId);

  //Here you aren't directly returning a user, but you are returning a function that will sometime in the future return a user. This is considered a promise.
  return userQuery.first
    ({
      success: function (userRetrieved) {
        //When the success method fires and you return userRetrieved you fulfill the above promise, and the userRetrieved continues up the chain.
        return userRetrieved;
      },
      error: function (error) {
        return error;
      }
    });
};
