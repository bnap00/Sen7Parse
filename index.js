// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
const resolve = require('path').resolve;
var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;
if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}
var remotedb = 'mongodb://admin:admin@ds019966.mlab.com:19966/sen7';
var localdb = 'mongodb://localhost/sen7'

var localServer = '10.100.57.25:1338'
var herokuServer = "sen7.herokuapp.com"


var server = localServer;
var api = new ParseServer({
  appName: "LocationServer",
  publicServerURL: process.env.PUBLIC_SERVER_URL || "http://"+localServer+"/",
  databaseURI: databaseUri || localdb,
  clientKey:"ClientKey24$",
  filesAdapter: {
    "module": "parse-server-fs-adapter",
    "options": {
      //"filesSubDirectory": "my/files/folder" // optional
    } 
  },
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',
  appId: process.env.APP_ID || 'LocationServer',
  masterKey: process.env.MASTER_KEY || 'TheMasterKey24$', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'http://'+localServer+'/parse',  // Don't forget to change to https if needed
  liveQuery: {
    classNames: ["TimelinePost"] // List of classes to support for query subscriptions
  }/*,
  emailAdapter: {
    module: "simple-parse-smtp-adapter",
    options: {
      fromAddress: 'webmaster.sen7@gmail.com',
      user: 'webmaster.sen7@gmail.com',
      password: 'sen4life',
      host: 'smtp.gmail.com',
      isSSL: true, //True or false if you are using ssl 
      port: 465, //SSL port or another port 
      //Somtimes the user email is not in the 'email' field, the email is search first in 
      //email field, then in username field, if you have the user email in another field 
      //You can specify here 
      emailField: 'email',
      templates: {
        passwordResetEmail: {
          subject: 'Reset your password',
          pathPlainText: resolve(__dirname, '/mail/reset-password.txt'),
          pathHtml: resolve(__dirname, '/mail/reset-password.html'),
          callback: (user) => { return { firstName: user.get('firstName') } }
          // Now you can use {{firstName}} in your templates
        },
        verificationEmail: {
          subject: 'Confirm your account',
          pathPlainText: resolve(__dirname, '/mail/verify_email.txt'),
          pathHtml: resolve(__dirname, '/mail/verify_email.html'),
          callback: (user) => { return { firstName: user.get('firstName') }}
          // Now you can use {{firstName}} in your templates
        },
      }
    }
  }*/
});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey
 var firebase = require('firebase');
var config = {
    apiKey: "AIzaSyC9fFLsb4rypm-m_zN_q5bexui-d-Pankg",
    authDomain: "locationclient-126ed.firebaseapp.com",
    databaseURL: "https://locationclient-126ed.firebaseio.com",
    storageBucket: "locationclient-126ed.appspot.com",
    messagingSenderId: "548625748542"
  };
  firebase.initializeApp(config);
var app = express();

// Serve static assets from the /public folder
app.use('/files/LocationServer', express.static(path.join(__dirname, '/files')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function (req, res) {
  res.status(200).send('Its API Page, Figure it out yourself');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', function (req, res) {
  res.sendFile(path.join(__dirname, '/public/test.html'));
});

app.use("/pages", require('./pages/main.js'));

var port = process.env.PORT || 1338;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function () {
  console.log('parse-server-example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
